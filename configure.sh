#!/bin/bash

echo "WARNING: This repository has been deprecated."
echo "XpServer is now called LightMill and has been moved to https://github.com/QuentinRoy/LightMill."
read -p "Do you want to automatically switch to LightMill now (y/n)? " answer
case ${answer:0:1} in
    y|Y )
        ./switch-to-lightmill.sh &&
        echo "Configuring LightMill..." &&
        ./configure.sh
    ;;
    * )
        echo "You can use ./switch-to-lightmill.sh to easily switch later if you change your mind."

        virtualenv venv && source ./venv/bin/activate && pip install -r requirements.txt
    ;;
esac
