#!/bin/bash

echo "Preparing your repository for git transition..."
echo "Backing up git's master branch to \"legacy-master\"..."
git branch -m master legacy-master &&
echo "Checking out and prepare transition branch..." &&
git checkout new-master &&
git branch -m new-master master &&

if [ $? -ne 0 ]; then
  echo "Failed... Sorry."
  exit -1
fi

read -p "Do you want to use SSH (if you did not set it up, SSH will fail)? (y/n) " answer
case ${answer:0:1} in
    y|Y )
        echo "Use git@github.com:QuentinRoy/LightMill.git."
        addr=git@github.com:QuentinRoy/LightMill.git
    ;;
    * )
        echo "Use https://github.com/QuentinRoy/LightMill.git."
        addr=https://github.com/QuentinRoy/LightMill.git
    ;;
esac

echo "Change origin's url."
git remote set-url origin $addr &&
echo "Track origin's master." &&
git branch -u origin/master master &&
echo "Pull!" &&
git pull &&
echo "Switch successful!"

if [ $? -ne 0 ]; then
  echo "Failed... Sorry."
  exit -1
fi

exit 0
